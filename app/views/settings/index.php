<?php
include "../controllers/settings.php";
$settings = new Settings();
$s = $settings->getFirst();
$navigatin_option = array(
	array(
		'value' => 'left',
		'title' => 'Left Navigation'
	),

	array(
		'value' => 'center',
		'title' => 'Center Navigation'
	),

	array(
		'value' => 'right',
		'title' => 'Right Navigation'
	),

	array(
		'value' => 'empty',
		'title' => 'No Navigation'
	)
);

$loader_option = array(
	array(
		'value' => 'RotatingPlain',
		'title' => 'Rotating Plain'
	),
	array(
		'value' => 'DoubleBounce',
		'title' => 'Double Bounce'
	),
	array(
		'value' => 'Wave',
		'title' => 'Wave'
	),
	array(
		'value' => 'WanderingCubes',
		'title' => 'Wandering Cubes'
	),
	array(
		'value' => 'FadingFour',
		'title' => 'Fading Four'
	),
	array(
		'value' => 'FadingCube',
		'title' => 'Fading Cube'
	),

	array(
		'value' => 'Pulse',
		'title' => 'Pulse'
	),
	array(
		'value' => 'ChasingDots',
		'title' => 'Chasing Dots'
	),
	array(
		'value' => 'ThreeBounce',
		'title' => 'Three Bounce'
	),
	array(
		'value' => 'Circle',
		'title' => 'Circle'
	),
	array(
		'value' => 'CubeGrid',
		'title' => 'Cube Grid'
	),
	array(
		'value' => 'FadingCircle',
		'title' => 'Fading Circle'
	),

	array(
		'value' => 'RotatingCircle',
		'title' => 'Rotating Circle'
	),
	array(
		'value' => 'FoldingCube',
		'title' => 'Folding Cube'
	),
	array(
		'value' => 'PumpingHeart',
		'title' => 'Pumping Heart'
	),
	array(
		'value' => 'DualRing',
		'title' => 'Dual Ring'
	),
	array(
		'value' => 'HourGlass',
		'title' => 'Hour Glass'
	),
	array(
		'value' => 'PouringHourGlass',
		'title' => 'Pouring Hour Glass'
	),

	array(
		'value' => 'FadingGrid',
		'title' => 'Fading Grid'
	),
	array(
		'value' => 'Ring',
		'title' => 'Ring'
	),
	array(
		'value' => 'Ripple',
		'title' => 'Ripple'
	),
	array(
		'value' => 'SpinningCircle',
		'title' => 'Spinning Circle'
	),
	array(
		'value' => 'SquareCircle',
		'title' => 'Square Circle'
	)
);

$title_option = array(
	array(
		'value' => 'text',
		'title' => 'Text'
	),
	array(
		'value' => 'image',
		'title' => 'Image'
	),
	array(
		'value' => 'empty',
		'title' => 'Empty'
	)
);


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$settings->setParams($_POST, $_FILES);
	$settings->update();
	$s = $settings->getById($s["id"]);
}
?>

<!-- Content Header (Page header) -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Settings</h1>
		<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
			<li class="breadcrumb-item active">Settings</li>
		</ol>
	</div>

	<div>
		<div class="card shadow mb-4">
			<!-- Card Header - Dropdown -->
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Settings</h6>
			</div>
			<!-- Card Body -->
			<form method="post" action="" id="form" enctype="multipart/form-data">
				<div class="card-body">



					<div class="row">
						<div class="col-md-6">
							<input type="hidden" id="id" name="id" value="<?= $s['id'] ?>">

							<div class="form-group">
								<label for="title">Title Header</label>
								<input type="text" class="form-control" id="title" name="title" placeholder="Title" value="<?= $s['title'] ?>" required>
							</div>
							<div class="form-group">
								<label for="url">Base URL</label>
								<input type="text" class="form-control" id="url" name="url" placeholder="URL" value="<?= $s['url'] ?>" required>
							</div>
  
							<!-- /.form-group -->

							<div class="form-group">
								<label for="loader">Select loader style</label>

								<div class="d-flex justify-content-between">
									<select class="form-control loader" id="loader" name="loader" style="width: 100%;">
										<?php
										foreach ($loader_option as $option) {
										?>
											<option value="<?= $option['value'] ?>" <? if ($option['value'] == $s['loader']) echo "selected" ?>><?= $option["title"] ?></option>
										<?php } ?>
									</select>
									<img src="../img/loading/<?= $s['loader'] ?>.gif" id="image_loader" style="height:40px; width: 40px; margin-left:5px" />
								</div>

							</div>
							<!-- /.form-group -->


							<div class="form-group">
								<label for="image">Logo</label>
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input" id="image" name="image" onChange="readURL(this);">
										<label class="custom-file-label" for="image">Choose file</label>
									</div>
								</div>
							</div>

							<div class="form-group">
								<img src="../images/settings/<?= $s["logo"] ?>?<?= time() ?>" style="width:100px; background-color: #bdbdbd" id="thumb_img" class="img-thumbnail">
							</div>



						</div>
						<!-- /.col -->
						<div class="col-md-6">

							<div class="form-group">
								<label for="sub_title">Sub Title</label>
								<input type="text" class="form-control" id="sub_title" name="sub_title" placeholder="Sub Title" value="<?= $s['sub_title'] ?>" required>
							</div>

							<div class="form-group">
								<label for="navigatin_bar_style">Select navigatin bar style</label>
								<select class="form-control navigatin_bar_style" id="navigatin_bar_style" name="navigatin_bar_style" style="width: 100%;">
									<?php
									foreach ($navigatin_option as $option) {
									?>
										<option value="<?= $option['value'] ?>" <? if ($option['value'] == $s['navigatin_bar_style']) echo "selected" ?>><?= $option["title"] ?></option>
									<?php } ?>
								</select>
							</div>
						
							<div class="form-group">
								<label for="type_header">Select type header </label>
								<select class="form-control" id="type_header" name="type_header" style="width: 100%;">
									<?php
									foreach ($title_option as $option) {
									?>
										<option value="<?= $option['value'] ?>" <? if ($option['value'] == $s['type_header']) echo "selected" ?>><?= $option["title"] ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label for="logo_header">Logo Header</label>
								<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input" id="logo_header" name="logo_header" onChange="readURLLogo(this);">
										<label class="custom-file-label" for="logo_header">Choose file</label>
									</div>
								</div>
							</div>

							<div class="form-group">
								<img src="../images/settings/<?= $s["logo_header"] ?>?<?= time() ?>" style="width:100px; background-color: #bdbdbd" id="thumb_img_logo" class="img-thumbnail">
							</div>

						</div>
						<!-- /.col -->
					</div>

					<button type="submit" class="btn btn-primary btn-icon-split">
						<span class="icon text-white-50">
							<i class="fas fa-save"></i>
						</span>
						<span class="text">Save</span>
					</button>

				</div>
			</form>
		</div>
	</div>
</div>






<script type="text/javascript">
	$(document).ready(function() {

		$("#loader").on('change', function() {
			$('#image_loader').attr('src', '../img/loading/' + this.value + '.gif');
		});

		$("#right_button").on('change', function() {
			$('#image_right_button').attr('src', '../img/button/' + this.value + '.png');
		});

		$("#left_button").on('change', function() {
			$('#image_left_button').attr('src', '../img/button/' + this.value + '.png');
		});

		$('#form').validate({
			rules: {
				title: {
					required: true,
				},
				url: {
					url: true
				}
			},
			messages: {
				title: {
					required: "Please enter a title",
				},
				url: {
					required: "Please enter a url",
					url: "Please enter valid url (http://www.example.com)",
				}
			},
			errorElement: 'div',
			errorPlacement: function(error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function(element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
</script>