<?php
$active = "";
$active_sub = "";
if (isset($_GET['page'])) {
  $action = $_GET['page'];
  $url = explode("_", $action);
  if (count($url) == 1) {
    $active = $url[0];
    $active_sub = $url[0] . "_list";
  } else {
    $active = $url[0];
    $active_sub = $url[0] . "_" . $url[1];
  }
} else {
  $active = "dashboard";
}

?>


<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../views/index.php">
    <div class="sidebar-brand-icon">
      <img src="../img/logo.png" width="40" />
    </div>
    <div class="sidebar-brand-text mx-3">flyWeb</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item <?= $active == 'dashboard' ? 'active' : '' ?>">
    <a class="nav-link" href="../views/index.php">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Interface
  </div>

  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item  <?= $active == 'menu' ? 'active' : '' ?> ">
    <a class=" nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
      <i class="fas fa-list"></i>
      <span>Menu Items</span>
    </a>
    <div id="collapseTwo" class="collapse <?= $active == 'menu' ? 'show' : '' ?> " aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Menu Items:</h6>
        <a class="collapse-item  <?= $active_sub == 'menu_list' ? 'active' : '' ?>" href="../views/index.php?page=menu">List Menu</a>
        <a class="collapse-item  <?= $active_sub == 'menu_add' ? 'active' : '' ?>" href="../views/index.php?page=menu_add">Add Menu</a>
      </div>
    </div>
  </li>

  <!-- Nav Item - Pages Collapse Social -->
  <li class="nav-item  <?= $active == 'social' ? 'active' : '' ?> ">
    <a class=" nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSocial" aria-expanded="true" aria-controls="collapseSocial">
      <i class="fas fa-share-square"></i>
      <span>Social Items</span>
    </a>
    <div id="collapseSocial" class="collapse <?= $active == 'social' ? 'show' : '' ?> " aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Social Media:</h6>
        <a class="collapse-item  <?= $active_sub == 'social_list' ? 'active' : '' ?>" href="../views/index.php?page=social">List Social</a>
      </div>
    </div>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Settings
  </div>

  <!-- Nav Item - Settings -->
  <li class="nav-item <?= $active == 'settings' ? 'active' : '' ?>">
    <a class="nav-link" href="../views/index.php?page=settings">
      <i class="fas fa-cogs"></i>
      <span>Settings</span></a>
  </li>

  <!-- Nav Item - Pages Collapse Social -->
  <li class="nav-item  <?= $active == 'icon' ? 'active' : '' ?> ">
    <a class=" nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseHeader" aria-expanded="true" aria-controls="collapseHeader">
      <i class="fas fa-bars"></i>
      <span>Header icon navigation</span>
    </a>
    <div id="collapseHeader" class="collapse <?= $active == 'icon' ? 'show' : '' ?> " aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Header navigation:</h6>
        <a class="collapse-item  <?= $active_sub == 'icon_left' ? 'active' : '' ?>" href="../views/index.php?page=icon_left">Left Icon</a>
        <a class="collapse-item  <?= $active_sub == 'icon_right' ? 'active' : '' ?>" href="../views/index.php?page=icon_right">Right Icon</a>
      </div>
    </div>
  </li>

  <!-- Nav Item - App color -->
  <li class="nav-item <?= $active == 'colors' ? 'active' : '' ?>">
    <a class="nav-link" href="../views/index.php?page=colors">
      <i class="fas fa-fw fa-wrench"></i>
      <span>App color</span></a>
  </li>

  <li class="nav-item <?= $active == 'application' ? 'active' : '' ?>">
    <a class="nav-link" href="../views/index.php?page=application">
      <i class="fas fa-mobile-alt"></i>
      <span>Application ID</span></a>
  </li>

  <li class="nav-item <?= $active == 'admob' ? 'active' : '' ?>">
    <a class="nav-link" href="../views/index.php?page=admob">
      <i class="fas fa-ad"></i>
      <span>adMob</span></a>
  </li>

  <li class="nav-item <?= $active == 'about' ? 'active' : '' ?>">
    <a class="nav-link" href="../views/index.php?page=about">
      <i class="fas fa-info-circle"></i>
      <span>About Us</span></a>
  </li>

  <li class="nav-item <?= $active == 'share' ? 'active' : '' ?>">
    <a class="nav-link" href="../views/index.php?page=share">
      <i class="fas fa-share-alt"></i>
      <span>Share Contnet</span></a>
  </li>

  <li class="nav-item  <?= $active == 'notification' ? 'active' : '' ?> ">
    <a class=" nav-link collapsed" href="#" data-toggle="collapse" data-target="#notification" aria-expanded="true" aria-controls="notification">
      <i class="fas fa-bell"></i>
      <span>Send notification</span>
    </a>
    <div id="notification" class="collapse <?= $active == 'notification' ? 'show' : '' ?> " aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Menu Items:</h6>
        <a class="collapse-item  <?= $active_sub == 'notification_list' ? 'active' : '' ?>" href="../views/index.php?page=notification">OneSignal Config</a>
        <a class="collapse-item  <?= $active_sub == 'notification_send' ? 'active' : '' ?>" href="../views/index.php?page=notification_send">Send notification</a>
      </div>
    </div>
  </li>


  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Profile
  </div>
  <!-- Nav Item - Logout -->
  <li class="nav-item <?= $active == 'profile' ? 'active' : '' ?>">
    <a class="nav-link" href="../views/index.php?page=profile">
      <i class="fas fa-user"></i>
      <span>Profile</span></a>
  </li>

  <!-- Nav Item - Logout -->
  <li class="nav-item">
    <a class="nav-link" href="../logout.php">
      <i class="fas fa-sign-out-alt"></i>
      <span>Logout</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>