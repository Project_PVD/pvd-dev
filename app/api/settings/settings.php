<?php

require_once("../../controllers/settings.php");

require_once("../../controllers/menus.php");

require_once("../../controllers/socials.php");

require_once("../../controllers/LeftNavigationIcon.php");

require_once("../../controllers/RightNavigationIcon.php");
 
$settings = new Settings();

$menus = new Menus();

$socials = new Socials();

$leftNavigationIcon = new LeftNavigationIcon();

$rightNavigationIcon = new RightNavigationIcon();
  
 
$settings_arr = array();
$settings_arr["data"] = $settings->getFirst();
$settings_arr["data"]["menus"]=$menus->getAllEnable();  
$settings_arr["data"]["socials"]=$socials->getAllEnable();  
$settings_arr["data"]["leftNavigationIcon"]=$leftNavigationIcon->getAllEnable();  
$settings_arr["data"]["rightNavigationIcon"]=$rightNavigationIcon->getAllEnable();


http_response_code(200);
echo json_encode($settings_arr);